import sqlite3
from person import Person


class Person_collection:
    """A simple collection representing set of Persons. This collection can be saved and loaded from a sqlite database."""
    def __init__(self):
        self.persons = set()
    
    def add_person(self, person: Person):
        """Add a person to the collection."""
        self.persons.add(person)
    
    def remove_person(self, person: Person):
        """Remove a person from the collection."""
        self.persons.remove(person)

    def get_person(self, first_name: str, last_name: str):
        """Find a person in the collection by first and last name."""
        for person in self.persons:
            if person.first_name == first_name and person.last_name == last_name:
                return person
        return None
    
    def edit_person(self, first_name: str, last_name: str):
        """Edit a person in the collection by first and last name."""
        person = self.get_person(first_name, last_name)
        if person is None:
            print("Osoba neexistuje")
        else:
            person.setFirstName(input("Zadejte nove jmeno: "))
            person.setLastName(input("Zadejte nove prijmeni: "))
            person.setAge(input("Zadejte novy vek: "))
            person.setPhoneNumber(input("Zadejte novy telefonni cislo: "))
    
    def __str__(self):
        return f"Person collection with {len(self.persons)} persons."
    
    def save(self):
        """Save the collection to a sqlite database."""
        connection = sqlite3.connect("persons.db")
        cursor = connection.cursor()
        cursor.execute("CREATE TABLE IF NOT EXISTS persons (first_name TEXT, last_name TEXT, age INTEGER, phone_number TEXT)")
        for person in self.persons:
            cursor.execute("INSERT INTO persons VALUES (?, ?, ?, ?)", (person.first_name, person.last_name, person.age, person.phone_number))
            connection.commit()
        connection.close()        
        
    def load(self):
        """Load the collection from a sqlite database."""
        connection = sqlite3.connect("persons.db")
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM persons")
        for row in cursor:
            print(row)
        connection.close()