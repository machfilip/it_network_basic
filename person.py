class Person:
    """A simple class representing a person."""

    def __init__(self, first_name, last_name, age, phone_number):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age
        self.phone_number = phone_number
    
    def __str__(self):
        return f"{self.first_name} {self.last_name} ({self.age}) ({self.phone_number})"
    
    def toString(self):
        return f"{self.first_name} {self.last_name} ({self.age}) ({self.phone_number})"
    
    def setFirstName(self, first_name):
        self.first_name = first_name

    def setLastName(self, last_name):
        self.last_name = last_name

    def setAge(self, age):
        self.age = age
    
    def setPhoneNumber(self, phone_number):
        self.phone_number = phone_number