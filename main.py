from functools import lru_cache
import sqlite3
import sys
from typing import Self
from person import Person
from person_collection import Person_collection  # import Person class from Person.py


def getMenuChoice():
    """Display menu with choices. User input is returned.
    :rtype: integer
    """
    print("1. Pridat osobu")
    print("2. Odebrat osobu")
    print("3. Vypsat vsechny osoby")
    print("4. Upravit osobu")
    print("5. Ulozit do databaze")
    print("6. Nacist z databaze")
    print("7. Konec")
    return input("Vyberte polozku z menu: ")  # return user input


def menu(person_collection: Person_collection):
    """Perform menu operations based on user input."""
    choice = getMenuChoice()
    if choice == "1":
        print("add_person()")
        person = Person()
        person_collection.add_person(person)
    elif choice == "2":
        print("remove_person()")
        person_collection.remove_person()
    elif choice == "3":
        print("print(person_collection)")
        print(person_collection)
    elif choice == "4":
        print("edit_person()")
        person_collection.edit_person()
    elif choice == "5":
        print("person_collection.save()")
        person_collection.save()
    elif choice == "6":
        print("person_collection.load()")
        person_collection.load()
    elif choice == "7":
        sys.exit(0)
    else:
        print("Neplatna volba")


def main():
    print("Sprava pojistenych!")
    person_collection = Person_collection()
    while True:
        menu()


if __name__ == "__main__":
    main()    
    print(record)
